<div align="center">
<img src="./logo.png" width="300" >
<h1>Processo Seletivo 20.2</h1>
<h2>Exercícios HTML</h2>
<h2><a href="https://pksasso.gitlab.io/aula-css-1-in/">Live here</a></h2>
</div>


Estilizar o código que vocês fizeram como tarefa da aula de HTML com os conhecimentos de CSS aprendidos. Usem e abusem da criatividade de vocês, essa tarefa é livre para estilizar do jeito que quiserem.

No final, o repositório deve conter dois arquivos:
  - index.html (cópia da atividade de HTML)
  - style.css (não esqueçam de linkar esse arquivo ao HTML)